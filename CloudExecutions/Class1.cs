﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudExecutions
{
    internal class CloudVersion
    {
        public string VersionLocation { get; set; }
        public int VersionNumber { get; set; }
    }
}
