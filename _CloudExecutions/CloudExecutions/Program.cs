﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;

namespace CloudExecutions
{
    class Program
    {
        static string[] sessions;
        static List<string> lines = new List<string>();
        static void Main(string[] args)
        {
            string cloudVersion = @"C:\sw\DigiCord_1.0-160-g39e2d7a-media_srv-SW-3346-35\cloud";
            string csvLocation = @"c:\sw\AutoExecutions\Dataset.csv";
            string batLocation = @"c:\sw\AutoExecutions\test2.bat";


            if (IsCSVReadable(csvLocation, cloudVersion))
            {
                Console.WriteLine("Read was succesful, now making a batch file");
                WritingBatch(batLocation);

                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Read failed, check if there was a problem in the csv location");
            }
            /*
            Console.WriteLine(CloudVersion);
            Console.WriteLine("Is this the cloud folder?");
            Console.WriteLine("Press 1 if yes or type the folder");
            string reply = Console.ReadLine();
            if (!reply.Equals("1"))
            {
                CloudVersion = reply;
            }
            Console.WriteLine(csvLocation);
            Console.WriteLine("Is this the csv location?");
            Console.WriteLine("Press 1 if yes or type the location");
            reply = Console.ReadLine();
            if (!reply.Equals("1"))
            {
                csvLocation = reply;
            }
            Console.WriteLine("Now trying to read the csv");
            */
        }
        private static string GetLatestCloudVersion()
        {
            //for now were talking about inputing the cloud version so no need to keep doing this
            Console.WriteLine();
            string root = @"C:\sw";
            string credentials = "media_srv-master";

            List<CloudVersion> cloudVersions = new List<CloudVersion>();
            string[] rootFolders = Directory.GetDirectories(root);

            foreach (string folder in rootFolders)
            {
                if (folder.Contains(credentials))
                {
                    CloudVersion temp = new CloudVersion();
                    temp.VersionLocation = folder;
                    temp.VersionNumber = int.Parse(folder.Substring(49));
                    cloudVersions.Add(temp);
                }
            }
            return "";

        }

        private static void WritingBatch(string batLocation)
        {
            Console.WriteLine("Now attempting to write and create the batch file");
            try
            {
                using (StreamWriter file = new StreamWriter(batLocation))
                {
                    foreach (string line in lines)
                    {
                        file.WriteLine(line);
                    }
                };
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed while writing the file, exception: " + ex.Message);
                throw;
            }

        }
        private static bool IsInputFine(string[] sessions, string cloudVersion)
        {
            bool result = true;
            string dummySession = "4e64aff2-9cff-4c92-a452-567833bedec1_29032022195249";
            int idLength = dummySession.Length;
            int i = 0;


            foreach(string session in sessions)
            {
                if(session.Length < idLength)
                {
                    Console.WriteLine("Faulty input! input num: " + i + ". session: " + session);
                    result = true;
                    break;
                }
                else
                {
                    lines.Add("cd " + cloudVersion);
                    lines.Add("cmd /c run_session.bat " + session);
                }
                i++;
            }
            return result; 
        }
        private static bool IsCSVReadable(string datasetLocation, string cloudVersion)
        {
            bool succesfulRead = false;
            try
            {
                using (TextFieldParser parser = new TextFieldParser(datasetLocation))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        sessions = parser.ReadFields();
                        //if the input
                        succesfulRead = IsInputFine(sessions, cloudVersion);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed while reading because of: " + ex.Message);
                succesfulRead = false;
            }
            return succesfulRead;
        }
    }
}
