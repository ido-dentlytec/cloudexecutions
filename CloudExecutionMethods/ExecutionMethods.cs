﻿using Microsoft.VisualBasic.FileIO;
using System.Windows;

namespace CloudExecutionMethods
{
    public class ExecutionMethods
    {
        public static void WritingBatch(string batLocation, List<string> lines)
        {
            Console.WriteLine("Now attempting to write and create the batch file");
            try
            {
                using (StreamWriter file = new StreamWriter(batLocation))
                {
                    foreach (string line in lines)
                    {
                        file.WriteLine(line);
                    }
                }
                Console.WriteLine("Batch was created");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed while writing the file, exception: " + ex.Message);
                throw;
            }

        }
        public static bool IsCSVReadable(string datasetLocation, string cloudVersion,string[] sessions, List<string> lines)
        {
            bool succesfulRead = false;
            try
            {
                //string sessionlist = File.ReadAllLines(datasetLocation);
                //sessionlist = File.ReadAllLines();



                using (TextFieldParser parser = new TextFieldParser(datasetLocation))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    while (!parser.EndOfData)
                    {
                        //Processing row
                        sessions = parser.ReadFields();
                        //if the input
                        succesfulRead = IsInputFine(sessions, cloudVersion, lines);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed while reading because of: " + ex.Message);
                succesfulRead = false;
            }
            return succesfulRead;
        }
        public static bool IsInputFine(string[] sessions, string cloudVersion, List<string> lines)
        {
            bool result = true;
            string dummySession = "4e64aff2-9cff-4c92-a452-567833bedec1_29032022195249";
            int idLength = dummySession.Length;
            int i = 0;


            foreach (string session in sessions)
            {
                if (!session.Equals(""))
                {
                    if (session.Length < idLength)
                    {
                        Console.WriteLine("Faulty input! input num: " + i + ". session: " + session);
                        result = false;
                        break;
                    }
                    else
                    {
                        lines.Add("cd " + cloudVersion);
                        lines.Add("cmd /c run_session.bat " + session);
                    }
                    i++;
                }

            }
            return result;
        }
        private static void GetLatestCloudVersion(string cloudVersion)
        {
        }
        public static string FindOldestCloudVersion()
        {
            List<int> cloudVersions = new List<int>();
            List<string> temp = new List<string>();

            string root = @"C:\sw";
            string credentials = "media_srv-master";
            string result = "";
            int highestVersion = 0;


            string[] possibleCloudFolders = Directory.GetDirectories(root);
            foreach (string folder in possibleCloudFolders)
            {
                if (folder.Contains(credentials))
                {
                    try
                    {
                        temp.Add(folder);
                        cloudVersions.Add(int.Parse(folder.Substring(49)));
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            if (cloudVersions.Count > 0)
            {
                highestVersion = cloudVersions.Max();
            }

            foreach (string cloudVersion in temp)
            {
                if (cloudVersion.Substring(49).Equals(highestVersion.ToString()))
                {
                    result = cloudVersion;
                    //CloudFoldertxt.Text = cloudVersion;
                }
            }
            return result;

        }
        public bool DoLocationsExist(string batLocation, string csvLocation, string folderLocation)
        {
            bool doesBatLocationExist;
            bool doesCSVLocationExist;
            bool doesCloudFolderExist;

            doesBatLocationExist = Directory.Exists(batLocation);
            doesCSVLocationExist = File.Exists(csvLocation);
            doesCloudFolderExist = Directory.Exists(folderLocation);
            if (doesBatLocationExist && doesCSVLocationExist && doesCloudFolderExist)
            {
                return true;
            }
            else
            {
                if (!doesBatLocationExist)
                {
                    //MessageBox.Show("Batch location doesn't exist");
                }
                else if (!doesCSVLocationExist)
                {
                    //MessageBox.Show("CSV isn't where specified");
                }
                else if (!doesCloudFolderExist)
                {
                    //MessageBox.Show("Cloud folder doesn't exist");
                }
                return false;
            }
        }
    }
}