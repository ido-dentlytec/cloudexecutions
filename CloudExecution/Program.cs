﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using CloudExecutionMethods;

namespace CloudExecutions
{
    class Program
    {
        static string[] sessions;
        static void Main(string[] args)
        {
            //List<string> sessions;

            List<string> lines = new List<string>();


            string cloudVersion = @"C:\sw\DigiCord_1.0-160-g39e2d7a-media_srv-SW-3346-35\cloud";
            string csvLocation = @"c:\sw\AutoExecutions\Dataset.csv";
            string batLocation = @"c:\sw\AutoExecutions\AutoExecution.bat";

            if (ExecutionMethods.IsCSVReadable(csvLocation, cloudVersion, sessions, lines))
            {
                ExecutionMethods.WritingBatch(batLocation, lines);
            }
            else
            {
                Console.WriteLine("Bruh");
            }
        }
    }
}
