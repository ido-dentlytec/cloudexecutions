﻿using Microsoft.VisualBasic.FileIO;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using CloudExecutionMethods;

namespace WPF_AutoExecutions
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool succesfulRead = false;
        string[] sessions;
        string folderLocation = @"C:\sw\DigiCord_1.0-182-gcdafc2a-media_srv-master-38";
        string csvLocation = @"c:\sw\AutoExecutions\Dataset.csv";
        string batLocation = @"c:\sw\AutoExecutions\AutoExecution.bat";
        string dummySession = "4e64aff2-9cff-4c92-a452-567833bedec1_29032022195249";
        int idLength;
        public MainWindow()
        {
            InitializeComponent();
            CloudFoldertxt.Text = folderLocation;
            CsvFiletxt.Text = csvLocation;
            BatFiletxt.Text = batLocation;
            idLength = dummySession.Length;

            folderLocation =ExecutionMethods.FindOldestCloudVersion();
            CloudFoldertxt.Text = folderLocation;
        }

        private void CloudFolder_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog openFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
             //txtEditor.Text = File.ReadAllText(openFileDialog.FileName);
             if(openFolderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                folderLocation = openFolderDialog.SelectedPath;
             CloudFoldertxt.Text = folderLocation;
        }
        private void CsvFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                //txtEditor.Text = File.ReadAllText(openFileDialog.FileName);
                csvLocation = openFileDialog.FileName;
            CsvFiletxt.Text = csvLocation;
        }
        private void BatFile_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog openFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            //txtEditor.Text = File.ReadAllText(openFileDialog.FileName);
            if (openFolderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                batLocation = openFolderDialog.SelectedPath;
            BatFiletxt.Text = batLocation;
        }

        private void StartOperation_Click(object sender, RoutedEventArgs e)
        {
            batLocation = BatFiletxt.Text;
            csvLocation = CsvFiletxt.Text;
            folderLocation = CloudFoldertxt.Text;

            bool faultyInput = false;
            List<string> lines = new List<string>();

            if (!batLocation.Contains(".bat"))
            {
                batLocation += "AutoExecution.bat";
            }

            if (!folderLocation.Contains(@"\cloud"))
            {
                folderLocation += @"\cloud";
            }

            succesfulRead = ExecutionMethods.IsCSVReadable(csvLocation, folderLocation, sessions, lines);



            if (succesfulRead)
            {
                ExecutionMethods.WritingBatch(batLocation, lines);
            }
            else
            {
                MessageBox.Show("Error in the csv");
                if (!faultyInput)
                {
                    MessageBox.Show("Read failed, check if there was a problem in the csv location");
                }
            }
        }
    }
}
